int parent[MAX_NODES];                  /*Keeps track of the parent of every vertex in the tree*/
bool visited[MAX_NODES];                
vector<vector<int> > tree[MAX_NODES];   /*The tree is stored as an undirected graph using an adjacency list*/



/*GetParents() function traverses the tree and computes the parent array such that
The pre-order traversal begins by calling GetParents(root_node,-1) */
void GetParents(int node , int par){       
    for(int i = 0 ; i < tree[node].size() ; ++i){
        /*As this is a pre-order traversal of the tree the parent of the current    node has already been processed*/
        Thus it should not be processed again*/
        if(tree[node][i] != par){
            parent[tree[node][i]] = node ; 
            GetParents(tree[node][i] , node) ; 
        }
    }
}


/*Computes the LCA of nodes u and v . */
int LCA(int u , int v){
    /*traverse from node u uptil root node and mark the vertices encountered along the path . */
    int lca ; 
    while(1){
        visited[u] = true ; 
        if(u == root_node){
            break ; 
        }
        u = parent[u] ; 
    }

    /*Now traverse from node v and keep going up untill we dont hit a node that is in the path of node u to root node*/
    while(1){
        if(visited[v]){ /*Intersection of paths found at this node.*/
            lca = v;    
            break ;  
        }
        v = parent[v] ; 
    }
    return lca ; 
}

//hackerrank
//Time Complexity Per Query: O(N)
//Time Complexity of processing : O(N+E)